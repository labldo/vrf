//      _    _       ______      _______
//       \  /       |_____/      |______
//        \/        |    \_      |      
//                                      
//
// Copyright 2017 abhi shelat
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//        http://www.apache.org/licenses/LICENSE-2.0
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.




// This package implements a verifiable random function used in the paper
//    "Micropayments for Decentralized Currencies"
//    Rafael Pass and abhi shelat
//    CCS 2015
//
// The function is defined as
//      pk: (g,g^x)
//      sk: x
//      Vrf(sk, msg) : H(msg)^sk, NIZK that (g,g^sk,H(msg,H(msg)^sk) \in DDH
//                     Technically, the output of the VRF should be H(H_G(msg)^sk)
//                     where H_g maps to the group, and the outside H is also an RO
//                     that maps to {0,1}^secparam
//
//      Verify(pk, msg, pi): verifies the NIZK
//
//
// This implementation uses group operations at the 128-bit security level; however, the
// scheme itself has only been analyzed asymptotically.  A concrete security analysis will
// involve a parameter for the number of queries that an adversary makes.
//
// 

//  VRF consists of the triple (H(msg)^x, (alpha,z))
//  Hash the message to the curve, and raise to the sk x.
//  This forms a DDH tuple:  (g, g^x, H(msg), H(msg)^x), and the proof
//  corresponds to a Schnorr sigma-protocol (squashed using Fiat-Shamir) as follows:
//  alpha = (g*H(msg))^{r}
//  c = hash(instance,alpha)
//  z = cx+r  
//  check that (gH(msg))^z = (g^x)^c(H(msg)^x)^c * alpha
//
// In a pairing-based curve, the DDH check can be replaced by the pairing as:
//
//   e(g1, g2^r) = e(g1^r, g2)
//
// This system essentially corresponds to the BLS signature scheme in the ROM.
//
// Alternatively, the DY function is a VRF under a q-bounded assumption.
package psvrf





import (
	"gitlab.com/abhvious/vrf"
	"crypto/rand"
	"crypto/sha256"
	"crypto/elliptic"
	"math/big"
	"log"
	"errors"
)


type PublicKey struct {
	px, py *big.Int
	G elliptic.Curve
}


type PrivateKey struct {
	PublicKey
	x *big.Int
}


// This method constructs a new SK object for the Vrf.
func NewPSVrf() vrf.VrfSK {
	var p PrivateKey
	p.G = elliptic.P256()

	p.x, p.px, p.py = makeRandomElement(p.G)

	return &p
}

// Returns the public key corresponding to a secret key.
func (sk *PrivateKey) GetPK() vrf.VrfPK {
	pk := new(PublicKey)
	pk.G = sk.G
	pk.px = sk.px
	pk.py = sk.py
	return pk
}

// Produces a []byte representation of the secret key for storage.
func (sk *PrivateKey) Marshal() []byte {
	plen := (sk.G.Params().BitSize + 7) >> 3
	b := make([]byte, plen)
	xb := sk.x.Bytes()
	copy( b[(plen-len(xb)):], xb )
	return b
}

// Reads a secret key from its Marshal version. Always succeeds since key is just 32b.
func (sk *PrivateKey) Unmarshal(b []byte) (vrf.VrfSK, error) {
	sk.G = elliptic.P256()
	sk.x = new(big.Int).SetBytes(b)
	return sk, nil
}


// Produces a []byte representation of a public key for storage, etc.
func (pk *PublicKey) Marshal() []byte {
	return elliptic.Marshal(pk.G, pk.px, pk.py)
}

// Reads a pk from []byte. Returns nil,error if the bytes do not correspond to a PK.
func (pk *PublicKey) Unmarshal(pkb []byte) (vrf.VrfPK, error) {
	pk.G = elliptic.P256()
	pk.px, pk.py = elliptic.Unmarshal(pk.G, pkb)
	if pk.px==nil || pk.py==nil { return nil, errors.New("Could not read PK") }

	return pk, nil
}


//  Applies the VRF to message msg. 
func (sk *PrivateKey) Vrf(msg []byte) ([]byte, []byte, error) {

	hm := sha256.Sum256(msg)

	hx,hy := hashToCurve(hm[:], sk.G)
	sigx,sigy := sk.G.ScalarMult(hx,hy,sk.x.Bytes())

	// This is the schnorr proof
	r := make([]byte, 32)
	rand.Read(r)
	ri := new(big.Int)
	ri.SetBytes(r)

	bx, by := sk.G.Add(hx, hy, sk.G.Params().Gx, sk.G.Params().Gy)

	ax, ay := sk.G.ScalarMult(bx,by,ri.Bytes()) 

	ci := challenge(msg, sigx, sigy, ax, ay)

	// compute response
	zi := new(big.Int)
	zi.Mul(ci, sk.x).Add(zi,ri).Mod(zi, sk.G.Params().N)

	plen := (sk.G.Params().BitSize + 7) >> 3

	sig := elliptic.Marshal(sk.G, sigx, sigy)
	prfp := elliptic.Marshal(sk.G, ax, ay)
	proof := make([]byte, len(prfp)+plen)

	zb := zi.Bytes()
	copy(proof[0:len(prfp)], prfp)
	copy(proof[len(prfp) + (plen-len(zb)):], zb)

	return sig,proof,nil

}



//  Checks that the Vrf value (sig) is the correct one for msg using proof.
func (pk *PublicKey) Verify(msg, sig, proof []byte) bool {
	ilen := (pk.G.Params().BitSize + 7) >> 3
	plen := ilen*2 + 1

	if len(sig)!=plen || len(proof)!=(plen+ilen) { return false }

	hm := sha256.Sum256(msg)
	hx,hy := hashToCurve(hm[:], pk.G)

	// sig is [px,py]
	// proof is [ax,ay,zi]
	// need to compute whether
	// g^zi = sig^c * (ax,ay)

	sigx, sigy := elliptic.Unmarshal(pk.G, sig)
	ax, ay := elliptic.Unmarshal(pk.G, proof[0:plen])

	zi := new(big.Int)
	zi.SetBytes(proof[ plen : plen+ilen ])

	if !pk.G.IsOnCurve(sigx, sigy) || !pk.G.IsOnCurve(ax,ay) { 
		log.Printf("Points are not on curve")
		return false 
	}

	// compute left-hand side of equation
	bx, by := pk.G.Add(hx,hy, pk.G.Params().Gx, pk.G.Params().Gy)
	lhx, lhy := pk.G.ScalarMult(bx, by, zi.Bytes())

	// compute right-hand side of equation
	ci := challenge(msg, sigx, sigy, ax, ay)


	tx, ty := pk.G.Add(pk.px, pk.py, sigx, sigy)

	tx,ty = pk.G.ScalarMult(tx, ty, ci.Bytes())
	rhx,rhy := pk.G.Add(tx,ty, ax, ay)

	if (lhx.Cmp(rhx)==0) && (lhy.Cmp(rhy)==0) { return true; }

	return false
}


// Helper method to generate the Fiat-Shamir challenge value.
func challenge(msg []byte, sigx, sigy, ax, ay *big.Int) (*big.Int) {
	h := sha256.New()
	h.Write(msg)
	h.Write(sigx.Bytes())
	h.Write(sigy.Bytes())
	h.Write(ax.Bytes())
	h.Write(ay.Bytes())
	cb := h.Sum(nil)

	ci := new(big.Int)
	ci.SetBytes(cb)
	return ci
}




// takes a hash value and finds a near curve point
// uses algorithm from relic/relic_ep_map.c
// This can be optimized
func hashToCurve(hx []byte, G elliptic.Curve) (x,y *big.Int) {
	x = new(big.Int)
	x.SetBytes(hx)
	y = new(big.Int)
	one := big.NewInt(1)
	x3  := new(big.Int)
	threeX := new(big.Int)
	// y² = x³ - 3x + b

	for {
	    x3.Mul(x,x)
    	x3.Mul(x3, x)
	    threeX.Lsh(x, 1)
	    threeX.Add(threeX, x)

	    x3.Sub(x3, threeX)
	    x3.Add(x3, G.Params().B)
	    x3.Mod(x3, G.Params().P)
	    // check if x3 is a square
    	if y.ModSqrt(x3, G.Params().P)!=nil {
    		break
    	}
    	x.Add(x,one)
	}

	return
}


func makeRandomElement(G elliptic.Curve) (xi, px, py *big.Int) {
	x := make([]byte, 32)
	_, err := rand.Read(x)
	if err!=nil {
		log.Fatal("Could not read random bytes")		
	}

	xi = new(big.Int)
	xi.SetBytes(x)

	px, py = G.ScalarMult(G.Params().Gx, G.Params().Gy, xi.Bytes())

	return xi, px, py
}
