//      _    _       ______      _______
//       \  /       |_____/      |______
//        \/        |    \_      |      
//                                      
//
// Copyright 2017 abhi shelat
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//        http://www.apache.org/licenses/LICENSE-2.0
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.


// Package psvrf provides an implementation of the DDH-based verifiable random function 
// described in "Micropayments for decentralized currencies."
// All of the unit tests are in this file.
package psvrf

import (
	"testing"
	"gitlab.com/abhvious/vrf"
	"crypto/rand"
)


func TestSingleVrf(t *testing.T) {
	sk := NewPSVrf()

	msg := make([]byte, 100)
	rand.Read(msg)
	sig, proof, err := sk.Vrf(msg)

	if err!=nil { t.Errorf("Could not produce Vrf value") }

	t.Logf("sample sig: %x\n  proof: %x\n",sig, proof)

	pk := sk.GetPK()

	if	!pk.Verify(msg, sig, proof) {
		t.Errorf("Could not verify sig/proof")
	}

}

func TestMarshall(t *testing.T) {
	sk := NewPSVrf()
	pk := sk.GetPK()

	skb := sk.Marshal()
	pkb := pk.Marshal()

	nsk, err := new(PrivateKey).Unmarshal(skb)

	if err!=nil { t.Errorf("Could not unmarshall sk") }

	npk, err := new(PublicKey).Unmarshal(pkb)

	if err!=nil { t.Errorf("Could not unmarshall pk") }

	msg := make([]byte, 100)
	rand.Read(msg)

	sig, proof, err := nsk.Vrf(msg)

	if err!=nil { t.Errorf("Could not sign from unmarshalled sk") }

	if	!npk.Verify(msg, sig, proof) {
		t.Errorf("Could not verify sig/proof from unmarshalled pk")
	}


}


func TestVrfLengths(t *testing.T) {
	sk := NewPSVrf()

	testVrf(t, sk)
}

func testVrf(t *testing.T, v vrf.VrfSK) {

	pk := v.GetPK()

	msg := make([]byte, 10000)
	rand.Read(msg)

	t.Logf("Testing len 1,...,10000")

	// test different message lengths
	for i:=1; i<10000; i++ {

		sig, proof, err := v.Vrf(msg[0:i])
		if err != nil {
			t.Errorf("Vrf error: %s", err)
			return
		}

		if !pk.Verify(msg[0:i], sig, proof) {
			t.Errorf("Verification error on msg len:%d [%x].\n", i, msg[0:i])
			t.Errorf("     sig: %x\n", sig)
			t.Errorf("   proof: %x\n", proof)

			return
		}
	}


}

func BenchmarkPSVrf(b *testing.B) {
	sk := NewPSVrf()
	benchmarkVrf(b, sk)
}
  
func benchmarkVrf(b *testing.B, v vrf.VrfSK) {


	msg := make([]byte, 400)  	


	b.ResetTimer()
	for i := 0; i < b.N; i++ {  		
		_, _, _ = v.Vrf(msg)
	}
}