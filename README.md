# Verifiable Random Function Library

This go package implements a verifiable random function used in the paper
    
    "Micropayments for Decentralized Currencies"
    Rafael Pass and abhi shelat
    https://eprint.iacr.org/2016/332 (full version)
    CCS 2015 (conference version)

The function is defined as:

	  pk: (g,g^x)
      sk: x
      Vrf(sk, msg) : H(msg)^sk, NIZK that (g,g^sk,H(msg,H(msg)^sk) \in DDH
      Verify(pk, msg, pi): verifies the NIZK


This implementation uses group operations at the 128-bit security level; however, the
scheme itself has only been analyzed asymptotically.  A concrete security analysis will
involve a parameter for the number of queries that an adversary will make.
